# Mike's Web3 Contact Form Ethereum Contract

Want to get ahold of Mike, reach out to him this way.

## Deploys to Ethereum

This is a solidity project that deploys to Ethereum blockchain. 

To get started, clone this and then install a bunch of stuff witn NPM:

```
npm init -y
npm install --save-dev hardhat
npm install --save-dev @nomiclabs/hardhat-waffle ethereum-waffle chai @nomiclabs/hardhat-ethers ethers
```

## Usage

A web application presents the users with the ability to connect their wallet (for identity) and then communicate with me. 

### First version is super simple and public

## Roadmap

1. Make it more private with encryption
2. Charge for different types of requests

# Hardhat readme

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts.

Try running some of the following tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```
