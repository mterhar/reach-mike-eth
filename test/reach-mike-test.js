const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Reacher", function () {
  it("Should return the reacher address on instantiation", async function () {
    const Reacher = await ethers.getContractFactory("ReachMike");
    const reacher = await Reacher.deploy();
    await reacher.deployed();

    expect(reacher.address).to.exist;
  });
});