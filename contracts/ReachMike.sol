// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract ReachMike {
    uint totalReaches;
    uint private seed;

    event newSup(address indexed from, uint timestamp, string message);

    struct OutReach {
        address reacher;
        string message;
        uint timestamp;
    }

    OutReach[] outreaches;

    mapping(address => uint) public lastSup;

    constructor() payable {
        console.log("I'm a contract smart enough to get you to Mike.");
    }

    function sup(string memory _message) public {
        // spam prevention
        require(lastSup[msg.sender] + 1 minutes < block.timestamp, "wait 1 minutes");
        lastSup[msg.sender] = block.timestamp;

        // if not spam, continue!
        totalReaches += 1;
        console.log("%s acknowledges Mike, message: %s", msg.sender, _message);
        outreaches.push(OutReach(msg.sender, _message, block.timestamp));
        emit newSup(msg.sender, block.timestamp, _message);

        uint randomNumber = (block.difficulty + block.timestamp + seed) % 100;
        console.log("Random number generated: %s", randomNumber);

        seed = randomNumber;

        if(randomNumber < 20){
            console.log("%s won!", msg.sender);
            uint prizeAmount = 0.0001 ether;
            require(prizeAmount <= address(this).balance, "Trying to withdraw more money than the contract has.");
            (bool success,) = (msg.sender).call{value: prizeAmount}(" ");
            require(success, "Failed to withdraw money from the contract.");
        }
        
    }

    function getAllReaches() view public returns (OutReach[] memory){
        return outreaches;
    }

    function getTotalReaches() view public returns (uint) {
        console.log("We have been acknowledged %s times", totalReaches);
        return totalReaches;
    }
}