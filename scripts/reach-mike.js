const hre = require("hardhat");

async function main(){
    await hre.run('compile');
    const [owner, rando] = await hre.ethers.getSigners();
    const Reacher = await hre.ethers.getContractFactory("ReachMike");
    const reacherContract = await Reacher.deploy({value: hre.ethers.utils.parseEther("1.0")});

    await reacherContract.deployed();

    console.log("reacher deployed to: ", reacherContract.address);
    console.log("reacher deployed by: ", owner.address)
    let contractBalance = await hre.ethers.provider.getBalance(reacherContract.address)
    console.log("reacher contract balance: ", hre.ethers.utils.formatEther(contractBalance))

    let reachCount;
    reachCount = await reacherContract.getTotalReaches();

    let acknowledgement = await reacherContract.sup("First test");
    await acknowledgement.wait();

    reachCount = await reacherContract.getTotalReaches();

    acknowledgement = await reacherContract.connect(rando).sup("rando test");
    await acknowledgement.wait();

    reachCount = await reacherContract.getTotalReaches();

    let allReaches = await reacherContract.getAllReaches();
    console.log(allReaches)

    contractBalance = await hre.ethers.provider.getBalance(reacherContract.address)
    console.log("reacher contract balance: ", hre.ethers.utils.formatEther(contractBalance))
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });