const hre = require("hardhat");

async function main(){
    await hre.run('compile');
    const [deployer] = await hre.ethers.getSigners();
    console.log("Deploying with account: ", deployer.address)
    console.log("Account balance: ", (await deployer.getBalance()).toString());
    
    const Reacher = await hre.ethers.getContractFactory("ReachMike");
    const reacherContract = await Reacher.deploy({value: hre.ethers.utils.parseEther("0.5")});

    await reacherContract.deployed();

    console.log("reacher deployed to: ", reacherContract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });